﻿using Dapper;
using DapperDemo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public class CompanyDapper : ICompanyRepository
    {
        private readonly IDbConnection _db;
        public CompanyDapper(IConfiguration configuration)
        {
            _db = new SqlConnection(configuration.GetConnectionString("Application"));
        }
        public CompanyModel Add(CompanyModel Model)
        {
            var sql = "INSERT INTO Companies(Name,Address,City,State,PostalCode) VALUES (@Name,@Address,@City,@State,@PostalCode);" + "SELECT CAST(SCOPE_IDENTITY() as int);";
            var id = _db.Query<int>(sql, Model).Single();
            
            Model.CompanyId = id;
            return Model;
        }

        public CompanyModel Find(int id)
        {
            var sql = "SELECT * FROM Companies WHERE CompanyId =" + id;
            return _db.Query<CompanyModel>(sql).Single();
        }

        public List<CompanyModel> GetAll()
        {
            var sql = "SELECT * FROM Companies;";
            return _db.Query<CompanyModel>(sql).ToList();
        }

        public void Remove(CompanyModel model)
        {
            var sql = "DELETE FROM Companies WHERE CompanyId = @CompanyId";
            _db.Execute(sql, new {CompanyId = model.CompanyId });
        }

        public CompanyModel Update(CompanyModel model)
        {
            var sql = "UPDATE Companies set Name = @Name ,Address = @Address," +
                " City = @City,State = @State, PostalCode = @PostalCode WHERE CompanyId = @CompanyId;";
            _db.Execute(sql, model);
            return model;

        }
    }
}
