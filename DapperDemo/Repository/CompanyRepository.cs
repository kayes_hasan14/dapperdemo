﻿using DapperDemo.Data;
using DapperDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly AppDbContext _context;

        public CompanyRepository(AppDbContext context)
        {
            _context = context;
        }
        public CompanyModel Add(CompanyModel Model)
        {
             _context.Companies.Add(Model);
            _context.SaveChanges();
            return Model;
        }

        public CompanyModel Find(int id)
        {
            return _context.Companies.Find(id);
        }

        public List<CompanyModel> GetAll()
        {
            return _context.Companies.ToList();
        }

        public void Remove(CompanyModel model)
        {
            
            _context.Companies.Remove(model);
            _context.SaveChanges();
        }

        public CompanyModel Update(CompanyModel model)
        {
            _context.Companies.Update(model);
            _context.SaveChanges();
            return model;
        }
    }
}
