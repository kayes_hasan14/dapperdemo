﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DapperDemo.Data;
using DapperDemo.Models;
using DapperDemo.Repository;

namespace DapperDemo.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyRepository _companyRepo;

        public CompanyController(ICompanyRepository companyRepo)
        {
            _companyRepo = companyRepo;
        }

        // GET: Company
        public IActionResult Index()
        {
            return View(_companyRepo.GetAll());
        }

        // GET: Company/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyModel = _companyRepo.Find(id.GetValueOrDefault());
            if (companyModel == null)
            {
                return NotFound();
            }

            return View(companyModel);
        }

        // GET: Company/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Company/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompanyId,Name,Address,City,State,PostalCode")] CompanyModel companyModel)
        {
            if (ModelState.IsValid)
            {
                _companyRepo.Add(companyModel);
               
                return RedirectToAction(nameof(Index));
            }
            return View(companyModel);
        }

        // GET: Company/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyModel = _companyRepo.Find(id.GetValueOrDefault());
           
            if (companyModel == null)
            {
                return NotFound();
            }
            return View(companyModel);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("CompanyId,Name,Address,City,State,PostalCode")] CompanyModel companyModel)
        {
            if (id != companyModel.CompanyId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
              _companyRepo.Update(companyModel);
                return RedirectToAction(nameof(Index));

            }
               
            
          return View(companyModel);
        }

        // GET: Company/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyModel = _companyRepo.Find(id.GetValueOrDefault());
          
            if (companyModel == null)
            {
                return NotFound();
            }

            return View(companyModel);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var companyModel =  _companyRepo.Find(id);
            _companyRepo.Remove(companyModel);
          
            return RedirectToAction(nameof(Index));
        }

        //private bool CompanyModelExists(int id)
        //{
        //    return _context.Companies.Any(e => e.CompanyId == id);
        //}
    }
}
