﻿using DapperDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperDemo.Repository
{
    public interface ICompanyRepository
    {
        CompanyModel Find(int id);
        List<CompanyModel> GetAll();
        CompanyModel Add(CompanyModel Model);
        CompanyModel Update(CompanyModel model);
        void Remove(CompanyModel model);
    }
}
